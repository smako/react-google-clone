export const initialState = {
  term: null,
  error: null,
};

export const actionTypes = {
  SET_SEARCH_TERM: "SET_SEARCH_TERM",
  REQUEST_SEARCH: "REQUEST_SEARCH",
  REQUEST_SEARCH_SUCCESS: "REQUEST_SEARCH_SUCCESS",
  REQUEST_SEARCH_ERROR: "REQUEST_SEARCH_ERROR",
};

const reducer = (state, action) => {
  switch (action.type) {
    case actionTypes.SET_SEARCH_TERM:
      return {
        ...state,
        term: action.term,
      };

    default:
      return state;
  }
};

export default reducer;
