import { useState, useEffect } from "react";
import { API_KEY, CONTEXT_KEY } from "./keys";
import Axios from "axios";

function useGoogleSearch(term) {
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      if (term) {
        Axios.get(`https://www.googleapis.com/customsearch/v1?key=${API_KEY}&cx=${CONTEXT_KEY}&q=${term}`)
          .then((response) => {
            let data = response.data;
            setData(data);
          })
          .catch((error) => {
            setError(error);
          });
      }
    };

    fetchData();
  }, [term]);

  return { data, error };
}

export default useGoogleSearch;
