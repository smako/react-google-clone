import React from "react";
import "./Footer.scss";
import { Link } from "react-router-dom";
import fetchLocationData from "../../fetchLocationData";

function Footer() {
  const { country } = fetchLocationData();
  return (
    <div className="footer">
      <div className="footer__top">{country}</div>
      <div className="footer__bottom">
        <div className="footer__bottom-left">
          {/* Link to Advertising */}
          <Link to="/advertising">Advertising</Link>
          {/* Link to Business */}
          <Link to="/business">Business</Link>
          {/* Link to About */}
          <Link to="/about">About</Link>
          {/* Link to How search works */}
          <Link to="/works">How search works</Link>
        </div>
        <div className="footer__bottom-right">
          {/* Link to Consumer Information */}
          <Link to="/consumer-information">Consumer Information</Link>
          {/* Link to Privacy */}
          <Link to="/privacy">Privacy</Link>
          {/* Link to Terms */}
          <Link to="/terms">Terms</Link>
          {/* Link to Settings */}
          <Link to="/settings">Settings</Link>
        </div>
      </div>
    </div>
  );
}

export default Footer;
