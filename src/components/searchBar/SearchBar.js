import React, { useState } from "react";
import "./SearchBar.scss";
import { Search as SearchIcon, Mic as MicIcon, Clear as ClearIcon } from "@material-ui/icons";
import { Button } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import { useStateValue } from "../../StateProvider";

function SearchBar({ hideButtons = false }) {
  const [{}, dispatch] = useStateValue("");
  const [input, setInput] = useState("");
  const history = useHistory();

  const clear = (e) => {
    e.preventDefault();
    setInput("");
  };

  const search = (e) => {
    e.preventDefault();
    history.push("/search");

    dispatch({
      type: "SET_SEARCH_TERM",
      term: input,
    });
  };

  return (
    <form className="searchBar" onSubmit={search}>
      <div className="searchBar__input">
        <SearchIcon className="searchBar__input-icon searchBar__input-iconSearch" />
        <input title="Search" value={input} onChange={(e) => setInput(e.target.value)} />
        {input && (
          <Button
            onClick={(e) => {
              e.preventDefault();
              setInput("");
            }}
            variant="text"
            className="searchBar__input-icon searchBar__input-iconClear"
          >
            <ClearIcon />
          </Button>
        )}
        <Button variant="text" className="searchBar__input-icon searchBar__input-iconMic">
          <MicIcon />
        </Button>
      </div>
      {!hideButtons && (
        <div className="searchBar__buttons">
          <Button variant="outlined" type="submit">
            Google Search
          </Button>
          <Button variant="outlined" type="submit">
            I'm Feeling Lucky
          </Button>
        </div>
      )}
    </form>
  );
}

export default SearchBar;
