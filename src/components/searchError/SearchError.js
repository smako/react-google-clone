import React from "react";
import "./SearchError.scss";
import { ArrowBack as ArrowBackIcon } from "@material-ui/icons";
import { Link } from "react-router-dom";

function SearchError({ error = "" }) {
  return (
    <div className="searchError">
      <h1>Oups a problem as been detected...</h1>
      <h3>
        <Link to="/">
          <ArrowBackIcon />
          Return to Home and try again
        </Link>
      </h3>
    </div>
  );
}

export default SearchError;
