import React from "react";
import "./SearchItem.scss";
import { ArrowDropDownRounded as ArrowDropDownIcon } from "@material-ui/icons";

function SearchItem({ item }) {
  return (
    <div className="searchItem__result">
      <a target="_blank" href={item.link} className="searchItem__result-url">
        {item.pagemap?.cse_image?.length > 0 && item.pagemap?.cse_image[0]?.src && <img className="searchItem__result-image" src={item.pagemap?.cse_image[0]?.src} alt="" />}
        {item.displayLink} <ArrowDropDownIcon />
      </a>
      <a target="_blank" className="searchItem__result-title" href={item.link}>
        <h2>{item.title}</h2>
      </a>
      <p className="searchItem__result-snippet">{item.snippet}</p>
    </div>
  );
}

export default SearchItem;
