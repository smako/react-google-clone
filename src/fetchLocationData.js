import { useState, useEffect } from "react";
import Axios from "axios";

const FetchLocationData = () => {
  const [country, setCountry] = useState("");

  useEffect(() => {
    const fetchData = async () => {
      Axios.get(`https://ipapi.co/json`).then((result) => {
        let data = result.data;
        setCountry(data.country_name);
      });
    };

    fetchData();
  }, []);

  return { country };
};

export default FetchLocationData;
