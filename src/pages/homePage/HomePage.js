import React from "react";
import "./HomePage.scss";
import { Link } from "react-router-dom";
import AppsIcons from "@material-ui/icons/Apps";
import Avatar from "@material-ui/core/Avatar";
import SearchBar from "../../components/searchBar/SearchBar";
import Footer from "./../../components/footer/Footer";

function Home() {
  return (
    <div className="home">
      <div className="home__header">
        <div className="home__header-left">
          {/* Link to About Page*/}
          <Link to="/about">About</Link>
          {/* Link to Store Page */}
          <Link to="/store">Store</Link>
        </div>
        <div className="home__header-right">
          {/* Link to Gmail Page */}
          <Link to="/gmail">Gmail</Link>
          {/* Link to Image Page */}
          <Link to="/images">Images</Link>
          {/* Icon */}
          <AppsIcons />
          {/* Avatar */}
          <Avatar />
        </div>
      </div>
      <div className="home__body">
        {/* Logo Google */}
        <img src="https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png" alt="" />
        <div className="home__body-container">
          {/* SearchBar */}
          <SearchBar />
        </div>
      </div>
      {/* Footer */}
      <Footer />
    </div>
  );
}

export default Home;
