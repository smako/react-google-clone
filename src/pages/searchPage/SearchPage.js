import React from "react";
import "./SearchPage.scss";
import { useStateValue } from "../../StateProvider";
import useGoogleSearch from "../../useGoogleSearch";
import { Link } from "react-router-dom";
import SearchBar from "../../components/searchBar/SearchBar";
import SearchItem from "../../components/searchItem/SearchItem";
import { Search as SearchIcon, Description as DescriptionIcon, Image as ImageIcon, LocalOffer as LocalOfferIcon, Room as RoomIcon, MoreVert as MoreVertIcon } from "@material-ui/icons";
import SearchError from "../../components/searchError/SearchError";
import Footer from "./../../components/footer/Footer";

function SearchPage() {
  const [{ term }] = useStateValue();
  const { data, error } = useGoogleSearch(term);

  if (error) return <SearchError />;
  else
    return (
      <div className="searchPage">
        <div className="searchPage__header">
          <Link to="/">
            {/* Logo Google */}
            <img className="searchPage__header-logo" src="https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png" alt="" />
          </Link>
          <div className="searchPage__header-body">
            <SearchBar hideButtons />
            <div className="searchPage__options">
              <div className="searchPage__options-left">
                <div className="searchPage__option">
                  <SearchIcon />
                  <Link to="/all">All</Link>
                </div>

                <div className="searchPage__option">
                  <ImageIcon />
                  <Link to="/images">Images</Link>
                </div>
                <div className="searchPage__option">
                  <DescriptionIcon />
                  <Link to="/news">News</Link>
                </div>
                <div className="searchPage__option">
                  <RoomIcon />
                  <Link to="/maps">Maps</Link>
                </div>
                <div className="searchPage__option">
                  <LocalOfferIcon />
                  <Link to="/shopping">Shopping</Link>
                </div>
                <div className="searchPage__option">
                  <MoreVertIcon />
                  <Link to="/more">More</Link>
                </div>
              </div>
              <div className="searchPage__options-right">
                <div className="searchPage__option">
                  <Link to="/setings">Settings</Link>
                </div>
                <div className="searchPage__option">
                  <Link to="/tools">Tools</Link>
                </div>
              </div>
            </div>
          </div>
        </div>
        {true && (
          <div className="searchPage__results">
            <p className="searchPage__result-count">
              <span>About {data?.searchInformation?.formattedTotalResults}</span>
              <span>Results ({data?.searchInformation?.formattedSearchTime} seconds)</span>
              <span>{term && `for "${term}"`}</span>
            </p>

            {data?.items.map((item, key) => (
              <SearchItem item={item} key={key} />
            ))}
          </div>
        )}
        {/* Footer */}
        <Footer />
      </div>
    );
}

export default SearchPage;
